package com.yjtc.cbg.chatdemo.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


import org.xutils.x;

/**
 * Created by chenboge on 16/4/29.
 */
public abstract class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
        initData();
    }

    public abstract void initData();


    public void startActivity(Intent intent,boolean isNeedLogin){

    }
}