package com.yjtc.cbg.chatdemo.View;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.yjtc.cbg.chatdemo.MainActivity;
import com.yjtc.cbg.chatdemo.R;
import com.yjtc.cbg.chatdemo.Widget.ClearEditText;
import com.yjtc.cbg.chatdemo.app.MyApplication;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * Created by chen on 2016/5/27.
 */
@ContentView(R.layout.login)
public class LoginActivity extends BaseActivity {

    @ViewInject(R.id.id_login_loginbtn)
    private Button mLogin;

    @ViewInject(R.id.id_login_uname)
    private ClearEditText mNameText;

    @ViewInject(R.id.id_login_pwd)
    private ClearEditText mPwdText;

    @ViewInject(R.id.id_login_touser)
    private ClearEditText mToUser;

    @ViewInject(R.id.id_tv_login_register)
    private TextView mRegisterText;


    @Event(value = R.id.id_login_loginbtn, type = View.OnClickListener.class)
    private void login(View view) {
        Toast.makeText(this, "开始登录", Toast.LENGTH_SHORT).show();
        String name = mNameText.getText().toString();
        String password = mPwdText.getText().toString();
        login(name,password);

    }

    private void login(String name,String password){
        AVUser.logInInBackground(name, password, new LogInCallback<AVUser>() {
            @Override
            public void done(AVUser avUser, AVException e) {
                System.out.println("uname:"+avUser.getUsername());
                System.out.println("email:"+avUser.getEmail());
                System.out.println("token:"+avUser.getSessionToken());
                MyApplication.getInstance().setmUser(avUser);
                MyApplication.getInstance().setToken(avUser.getSessionToken());
                MyApplication.getInstance().setToUser(mToUser.getText().toString());
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });
    }

    @Override
    public void initData() {

    }


    @Event(value = R.id.id_tv_login_register)
    private void register(View view){
        startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
    }
}
