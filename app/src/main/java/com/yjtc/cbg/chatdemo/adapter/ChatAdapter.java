package com.yjtc.cbg.chatdemo.adapter;

import android.content.Context;

import com.yjtc.cbg.chatdemo.R;

import java.util.List;

/**
 * Created by chen on 2016/5/28.
 */
public class ChatAdapter extends BaseAdapter<String, BaseViewHolder> {

    public ChatAdapter(Context mContext, List<String> mDatas, int itemViewID) {
        super(mContext, mDatas, itemViewID);
    }

    @Override
    void bindData(BaseViewHolder holder, int position) {
        holder.getTextView(R.id.id_text).setText(mDatas.get(position));
    }
}
