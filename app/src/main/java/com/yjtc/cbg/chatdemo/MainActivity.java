package com.yjtc.cbg.chatdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.yjtc.cbg.chatdemo.Util.ToastUtil;
import com.yjtc.cbg.chatdemo.View.BaseActivity;
import com.yjtc.cbg.chatdemo.Widget.ClearEditText;
import com.yjtc.cbg.chatdemo.adapter.ChatAdapter;
import com.yjtc.cbg.chatdemo.app.MyApplication;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    private RecieveMessageReciver mReciver;

    @ViewInject(R.id.chat_send_content)
    private EditText mContentText;

    @ViewInject(R.id.chat_buttom_send)
    private Button mSendText;

    @ViewInject(R.id.chat_content)
    private RecyclerView mView;

    private AVUser mUser;

    private ChatAdapter mAdapte;

    private String toUser;

    private List<String> mDatas=new ArrayList<>();

    @Override
    public void initData() {
        for(int i=0;i<3;i++){
            mDatas.add("Tom猫，起床吃饭了");
        }
        ToastUtil.showToast(this,"数据加载完成");
        toUser = MyApplication.getInstance().getToUser();
        mUser = MyApplication.getInstance().getmUser();
        mAdapte = new ChatAdapter(this, mDatas, R.layout.template_text);
        mView.setAdapter(mAdapte);
        mView.setLayoutManager(new LinearLayoutManager(this));
    }


    @Event(value = R.id.chat_buttom_send)
    private void sned(View view){
        sendMessageToJerryFromTom(mContentText.getText().toString());
    }


    public void sendMessageToJerryFromTom(final String message) {
        // Tom 用自己的名字作为clientId，获取AVIMClient对象实例
        AVIMClient tom = AVIMClient.getInstance(mUser.getUsername());
        // 与服务器连接
        tom.open(new AVIMClientCallback() {
            @Override
            public void done(AVIMClient client, AVIMException e) {
                if (e == null) {
                    // 创建与Jerry之间的对话
                    client.createConversation(Arrays.asList(toUser), mUser.getUsername()+" & "+toUser, null,
                            new AVIMConversationCreatedCallback() {

                                @Override
                                public void done(AVIMConversation conversation, AVIMException e) {
                                    if (e == null) {
                                        AVIMTextMessage msg = new AVIMTextMessage();
                                        msg.setText(message);
                                        // 发送消息
                                        conversation.sendMessage(msg, new AVIMConversationCallback() {

                                            @Override
                                            public void done(AVIMException e) {
                                                if (e == null) {
                                                    Log.d(mUser.getUsername()+" & "+toUser, "发送成功！");
                                                    mAdapte.AddData(mAdapte.getDatas().size(),mContentText.getText().toString());
                                                    mView.scrollToPosition(mAdapte.getDatas().size());
                                                    mContentText.setText("");
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                }
            }
        });
    }

    public  class RecieveMessageReciver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String s = intent.getStringExtra("msg");
            int itemsize = mAdapte.getDatas().size();
            mAdapte.AddData(itemsize,s);
            mView.scrollToPosition(mAdapte.getDatas().size());
          //  mAdapte.notifyDataSetChanged();
        }
    }
}
