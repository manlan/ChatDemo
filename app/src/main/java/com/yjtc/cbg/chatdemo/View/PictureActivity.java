package com.yjtc.cbg.chatdemo.View;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yjtc.cbg.chatdemo.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chen on 2016/5/27.
 */
@ContentView(R.layout.picture)
public class PictureActivity extends BaseActivity {

    @ViewInject(R.id.id_page)
    private TextView mtext;

    @ViewInject(R.id.id_pager)
    private ViewPager mPager;

    private MyAdapter mAdapter;

    private List<View> mViews=new ArrayList<>();

    @Override
    public void initData() {

        for(int i=0;i<10;i++){
            ImageView view = new ImageView(this);
            view.setImageResource(R.mipmap.guide1);
            mViews.add(view);
        }
        mAdapter = new MyAdapter(mViews);
        mPager.setAdapter(mAdapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mtext.setText(position+1+"/"+mViews.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    class MyAdapter extends PagerAdapter{

        private List<View> mDatas;

        public MyAdapter(List<View> mDatas) {
            this.mDatas = mDatas;
        }

        @Override
        public int getCount() {
            return mDatas.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(mDatas.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mDatas.get(position),0);
            return mDatas.get(position);
        }
    }
}
