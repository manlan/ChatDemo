package com.yjtc.cbg.chatdemo.View;

import android.content.Intent;
import android.view.View;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SignUpCallback;
import com.yjtc.cbg.chatdemo.MainActivity;
import com.yjtc.cbg.chatdemo.R;
import com.yjtc.cbg.chatdemo.Util.ToastUtil;
import com.yjtc.cbg.chatdemo.Widget.ClearEditText;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * Created by chen on 2016/5/27.
 */
@ContentView(R.layout.register)
public class RegisterActivity extends BaseActivity {


    @ViewInject(value = R.id.id_register_uname)
    private ClearEditText mUnameText;

    @ViewInject(value = R.id.id_register_password)
    private ClearEditText mPassWordText;

    @Override
    public void initData() {

    }


    //用户注册
    @Event(value = R.id.id_register)
    private void Register(View view){
        String uname = mUnameText.getText().toString();
        String password = mPassWordText.getText().toString();
        register(uname,password);
    }

    private void register(String namae,String password){
        AVUser user = new AVUser();// 新建 AVUser 对象实例
        user.setUsername(namae);// 设置用户名
        user.setPassword(password);// 设置密码
        user.setEmail("haozi@baigege.com");// 设置邮箱
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    // 注册成功
                    ToastUtil.showToast(RegisterActivity.this,"注册成功");
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                } else {
                    // 失败的原因可能有多种，常见的是用户名已经存在。
                    ToastUtil.showToast(RegisterActivity.this,"注册失败");
                    System.out.println("error:"+e.getCode()+"     message"+e.getMessage());
                }
            }
        });
    }
}
